﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Homework40
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var posts = db.Posts.Include(x => x.User).ToList();

                foreach (var x in posts)
                {
                    Console.WriteLine("+----------------------------------------------");
                    Console.WriteLine($"| FullName: {x.User.FirstName} {x.User.LastName} / Login: {x.User.Login}");
                    Console.WriteLine($"| Message: {x.Commentary}");
                    Console.WriteLine("+----------------------------------------------");

                }
            }
        }
    }
}
