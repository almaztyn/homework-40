﻿using Homework40.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework40.Models
{
    public class Post : IEntity<int>
    {
        public int Id { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public string Commentary { get; set; }
        public virtual User User { get; set; }
        
    }
}
